﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Channels;
using System.Reflection;
using Console = System.Console;
using System.Diagnostics;

namespace MAPZ2
{
    public class Stopwatch
    {
        private TimeSpan StartTime { get;  set; }
        private TimeSpan StopTime { get;  set; }
        private bool IsStarted { get; set; }
        public Stopwatch()
        {
            IsStarted = false;
        }

        public void Start()
        {
            if (!IsStarted)
            {
                IsStarted = true;
                StartTime = DateTime.Now.TimeOfDay;
            }
        }

        public void Stop()
        {
            if (IsStarted)
            {
                IsStarted = false;
                StopTime = DateTime.Now.TimeOfDay;
            }
        }

        public TimeSpan GetTime()
        {
            return StopTime - StartTime;
        }
    }


    class Program
    {
        static void Main()
        {
            var stopwatch = new Stopwatch();
            var reflectionsTest = new ReflectionsTest();
            List<MemberInfo> memberInfos = new List<MemberInfo>(typeof(ReflectionsTest).GetMembers());

            stopwatch.Start();
            for (int i = 0; i < 1000000; i++)
            {
                ((MethodInfo)memberInfos[0]).Invoke(reflectionsTest, new object[] { 6 });
            }
            stopwatch.Stop();
            Console.WriteLine("Reflection method call: {0}", stopwatch.GetTime());

            stopwatch.Start();
            for (int i = 0; i < 1000000; i++)
            {
                reflectionsTest.FirstMethod(6);
            }
            stopwatch.Stop();
            Console.WriteLine("Default method call: {0}", stopwatch.GetTime());

            ConstructorInfo info = typeof(ReflectionsTest).GetConstructor(new Type[] { typeof(int), typeof(string) });

            ReflectionsTest reflectionsTestObject;
            Console.WriteLine();

            stopwatch.Start();
            for (int i = 0; i < 1000000; i++)
            {
                reflectionsTestObject = (ReflectionsTest)info.Invoke(new object[] { 10, "text" });
            }
            stopwatch.Stop();
            Console.WriteLine("Reflection object creating : {0}", stopwatch.GetTime());

            stopwatch.Start();
            for (int i = 0; i < 1000000; i++)
            {
                reflectionsTestObject = new ReflectionsTest(10, "text");
            }
            stopwatch.Stop();
            Console.WriteLine("Default object creating: {0}", stopwatch.GetTime());
            Console.WriteLine();
            Console.WriteLine("(Time for one million instances)");
            Console.WriteLine("{0}, {1}", reflectionsTestVars.nField, reflectionsTestVars.sField)
        }
    }
}