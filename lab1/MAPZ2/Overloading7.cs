﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ2
{
    public class User1
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public User1(int Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
        }

        public User1()
        {
            this.Id = 0;
            this.Name = "User";
        }
    }

    class PremiumUser : User1
    {
        public string Status { get; set; }

        public PremiumUser(int Id, string Name, string Status)
            : base(Id, Name)
        {
            this.Status = Status;
        }
    }

    class SumCounter
    {
        int GetSum(int x, int y)
        {
            return x + y;
        }

        int GetSum(int x, int y, int z)
        {
            return x + y + z;
        }
    }

    class MultipleSumCounter : SumCounter
    {
        int GetSum(int x, int y, int z, int i)
        {
            return x + y + z + i;
        }
    }
}
