﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ2
{
    struct StructTest
    {
        int field1;
        void method1()
        {
            Console.WriteLine();
        }
    }

    class ClassTest
    {
        int field1;
        void method1()
        {
            Console.WriteLine();
        }
    }

    interface ITest
    {
        int field1 { get; set; }
        void method1();
    }
}
