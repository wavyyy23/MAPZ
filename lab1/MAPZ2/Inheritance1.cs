﻿
namespace MAPZ2
{
    public interface ICreature
    {
        public double Age { get; set; }
    }
    public abstract class Mammal : ICreature
    {
        public double Age { get; set; }
        public int? PawsAmount { get; set; }
    }
    public class Dog : Mammal
    {
        public string Name { get; set; }
    }
}