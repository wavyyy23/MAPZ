﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ2
{
    public interface IWalkable{ public void Walk(); }
    public interface ITalkable{ public void Talk(); }
    public class Walkable : IWalkable
    {
        public void Walk()
        {
            Console.WriteLine("I am walking!");
        }
    }
    public class Talkable : ITalkable
    {
        public void Talk()
        {
            Console.WriteLine("I am talking!");
        }
    }
    public class Human : IWalkable, ITalkable
    {
        public IWalkable iWalkable = new Walkable();
        public ITalkable iTalkable = new Talkable();

        public void Walk()
        {
            iWalkable.Walk();
        }

        public void Talk()
        {
            iTalkable.Talk();
        }
    }

}
