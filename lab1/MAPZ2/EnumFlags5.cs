﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ2
{
    [Flags]
    public enum Agreement
    {
    StronglyDisagree = 0b_0000_0000, //0
    MostlyDisagree = 0b_0000_0001, //1
    Neutral = 0b_0000_0010, //2
    MostlyAgree = 0b_0000_0100, //4
    StronglyAgree = 0b_0000_1000 //8
    }

    /*
     Agreement agreement = Agreement.StronglyDisagree & Agreement.MostlyDisagree; //0
            Agreement agreement1 = Agreement.Neutral | Agreement.MostlyAgree | Agreement.StronglyDisagree; //6
            Agreement agreement2 = Agreement.MostlyDisagree ^ Agreement.MostlyAgree; //5
            bool boolAgreement1 = Agreement.Neutral + 4 == agreement1; //true
            bool boolAgreement2 = Agreement.StronglyAgree == agreement1; //false
            Console.WriteLine((int)agreement);
            Console.WriteLine((int)agreement1); Console.WriteLine((int)agreement2);
            Console.WriteLine(boolAgreement1);
            Console.WriteLine(boolAgreement2);
     */
}
