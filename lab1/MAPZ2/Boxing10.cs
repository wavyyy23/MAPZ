﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ2
{
    class BoxingUnboxing
    {
        public void Test()
        {
            int a = 5;
            object b = a; //boxing
            int c = (int)b; //unboxing
        }
    }
}
