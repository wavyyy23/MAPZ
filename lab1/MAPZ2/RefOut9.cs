﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ2
{
    class RefTest
    {
        public static void IncrementArray(ref int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i]++;
            }
        }
    }
    class OutTest
    {
        int GetSumAndSub(int a, int b, out int sub)
        {
            sub = a - b;
            return a + b;
        }
    }
}
