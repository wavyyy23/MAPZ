﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ2
{
    class User
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string LastName { get; private set; }
        public string Password { get; private set; }

        public User(string name, string lastName, int id = 0, string password = "1234")
        {

            Name = name;
            LastName = lastName;
            Id = id;
            Password = password;
        }

        public static explicit operator User(UserDto userDto)
        {
            return new User(userDto.Name, userDto.LastName);
        }
    }

    class UserDto
    {
        public string Name { get; private set; }
        public string LastName { get; private set; }

        public UserDto(string name, string lastName)
        {
            Name = name;
            LastName = lastName;
        }

        public static implicit operator UserDto(User user)
        {
            return new UserDto(user.Name, user.LastName);
        }

    }
}
