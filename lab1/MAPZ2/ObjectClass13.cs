﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ2
{
    public class Home
    {
        public int countOfMembers { get; private set; }
        public double square { get; private set; }
        public string adress { get; private set; }

        public Home(int countOfMembers, double square, string adress)
        {
            this.countOfMembers = countOfMembers;
            this.square = square;
            this.adress = adress;
        }

        public override bool Equals(object otherObject)
        {
            Home home = (Home)otherObject;
            return (countOfMembers == home.countOfMembers) && (square == home.square) && (adress == home.adress);
        }
        public new static bool Equals(object first, object second)
        {
            return first.Equals(second);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return $"{countOfMembers}, {square}, {adress}";
        }
        public new Type GetType()
        {
            return base.GetType();
        }
        public static new bool ReferenceEquals(object objA, object objB)
        {
            return objA == objB;
        }

        protected new object MemberwiseClone()
        {
            return base.MemberwiseClone();
        }
    }

}

/*
 * Home homeFirst = new Home(5, 10, "Lukasha");
            Home homeSecond = new Home(5, 10, "Lukasha");
            Home homeThird = homeFirst;

            Console.WriteLine("1 and 2 objects are equal:" + Object.ReferenceEquals(homeFirst, homeSecond));
            Console.WriteLine("1 and 2 objects have same values:" + Object.Equals(homeFirst, homeSecond));
            Console.WriteLine("1 and 3 objects are equal:" + Object.ReferenceEquals(homeFirst, homeThird));

            Console.WriteLine("Home's value is: " + homeFirst.ToString());
            Console.WriteLine("Object type: " + homeFirst.GetType());
            Console.WriteLine("Object hashcode: " + homeFirst.GetHashCode());
 */